# README #

Evaluation of the QoS of SOSM in a small testbed. 
Traces has been collected following the next sequence of incoming tasks:

------ Start with the system empty and all the resources already registered -----

1.- Raytracer (QoS: High performance)- with web front end -> this will show how we deploy a container in the MIC  and a VM in OpenStack.

2.- Raytracer (QoS: no description) - as the Mic is busy  this both will go to the CPU node

3.- Genomics (QoS: high performance)- This will go to DFE

4.- Genomics (QoS: low performance) - This will go to the CPU

5.- Genomics (QoS: no description) - The sosm system will decide based in the SI

6.- Dense Matrix (QoS: no description) - The sosm system will deploy the CPU version

7.- Oil and Gas (QoS: no description) - The sosm system will deploy in the CPU node

8.- Raytracer (QoS: High performance) - with web frontend -> this should reject the task as the MIC still busy.

---- finalisation of raytracer [1]. 

9.- Redeploy 8: this goes to the MIC based in the SI and the availability of the MIC and to a VM in OpenStack.

------ END -----

